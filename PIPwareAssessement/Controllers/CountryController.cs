﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PIPwareAssessement.Models;
using PIPwareAssessement.Services;

namespace PIPwareAssessement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountryController : ControllerBase
    {
        private readonly ICountryService _countryService;

        public CountryController(ICountryService countryService)
        {
            _countryService = countryService;
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetCountriesAsync()
        {
            try
            {
                dynamic result = await _countryService.GetCountriesAsync();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("[action]")]
        public IActionResult DeleteByIso2Code(string iso2Code)
        {
            try
            {
                string result = _countryService.DeleteByIso2Code(iso2Code);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("[action]")]
        public IActionResult AddCountry(Country country)
        {
            try
            {
                string result = _countryService.AddCountry(country);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}