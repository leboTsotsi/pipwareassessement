﻿using System;
using Microsoft.EntityFrameworkCore;
using PIPwareAssessement.Models;

namespace PIPwareAssessement.Data
{
    public class CountryDashboardDBContext : DbContext
    {
        public CountryDashboardDBContext(DbContextOptions<CountryDashboardDBContext> options) : base(options)
        {
        }

        public DbSet<Country> Countries { get; set; }
    }
}
