﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net.Http;
using PIPwareAssessement.Data;
using PIPwareAssessement.Models;
using System.Threading.Tasks;
using System;

namespace PIPwareAssessement.Services
{

    public class CountryService : ICountryService
    {
        private readonly CountryDashboardDBContext _db;
        private readonly HttpClient _httpClient;
        private const string PATH = "country";

        public CountryService(CountryDashboardDBContext db, IHttpClientFactory clientFactory)
        {
            _db = db;
            _httpClient = clientFactory.CreateClient(name: "worldbank");
        }

        public async Task<dynamic> GetCountriesAsync()
        {
            IList<Country> countries = _db.Countries.ToList();
            string queryString = string.Join(";", countries.Select(c => c.iso2Code).ToList());

            if (!string.IsNullOrEmpty(queryString))
            {
                HttpResponseMessage response = await _httpClient.GetAsync($"{PATH}/{queryString.ToLower()}?format=json");
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsAsync<dynamic>();
                }
                throw new Exception(response.ReasonPhrase);
            }
            return countries;
        }

        public string DeleteByIso2Code(string iso2Code)
        {
            Country country = _db.Countries.SingleOrDefault(c => c.iso2Code == iso2Code);
            _db.Remove(country);
            _db.SaveChanges();
            return "Country Deleted successfully";
        }

        public string AddCountry(Country country)
        {
            _db.Countries.Add(country);
            _db.SaveChanges();
            return "A new country was added successfully";
        }
    }
}
