﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PIPwareAssessement.Models;

namespace PIPwareAssessement.Services
{
    public interface ICountryService
    {
        Task<dynamic> GetCountriesAsync();
        string DeleteByIso2Code(string iso2Code);
        string AddCountry(Country country);
    }
}
