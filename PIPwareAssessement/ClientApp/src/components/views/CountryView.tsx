import { Grid, Paper } from "@material-ui/core";
import React, { FunctionComponent } from "react";
import styled from "styled-components";
import { Country } from "../../store/countries";
import CountryList from "../lists/CountryList";
import CountryListItem from "../listItems/CountryListItem";
import isEmpty from "lodash/isEmpty";

const StyledPaper = styled(Paper)`
  && {
    margin: 5px;
    border: 0;
    border: 0.5px solid #f9f9f9;
  }
`;

const MainGrid = styled(Grid)`
  padding: 5px;
`;

interface OwnProps {
  countries: Country[];
  onCountryDelete: (countryId: string) => void;
}

type AllProps = OwnProps;

const CountryView: FunctionComponent<AllProps> = ({
  countries,
  onCountryDelete,
}) => {
  return (
    <StyledPaper>
      <MainGrid container={true} direction="row">
        {!isEmpty(countries) ? (
          Object.keys(countries).map((key: any) => {
            const country: Country = countries[key];
            return (
              <Grid
                lg={4}
                md={4}
                sm={4}
                xs={12}
                container={true}
                item={true}
                direction="column"
                key={key}
              >
                <CountryList>
                  <CountryListItem
                    {...{ country, onCountryDelete }}
                    key={key}
                  />
                </CountryList>
              </Grid>
            );
          })
        ) : (
          <></>
        )}
      </MainGrid>
    </StyledPaper>
  );
};

export default CountryView;
