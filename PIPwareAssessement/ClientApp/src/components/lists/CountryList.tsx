import React from "react";

import List, { ListProps } from "@material-ui/core/List";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import styled from "styled-components";

const ListContainer = styled(List)`
  && {
      display: flex;
      flex-direction: column;
      justify-content: center;
      max-width: 100%;
      width: 100%;
  }
`;

const CountryList: React.FunctionComponent<ListProps> = (props: ListProps) => {
  return <ListContainer {...props} />;
};

export default CountryList;
