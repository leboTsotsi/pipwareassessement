import React, { FunctionComponent } from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { CountryModel } from "../../store/countries";
import styled from "styled-components";
import { Typography } from "@material-ui/core";
import { countryToFlag } from "../../utils/general";

const StyledCountryWrapper = styled.div`
  display: flex;
`;

const CountryText = styled(Typography)`
  && {
    font-family: MuseoSans-700;
    font-size: 16px;
    color: #4c4c4e;
    line-height: 28px;
    margin-left: 5px;
    margin-right: 5px;
    margin-top: -2.5px;
  }
`;

const StyledAutocomplete = styled(Autocomplete)`
  && {
    width: 400px;
  }
`;

interface OwnProps {
  countries: CountryModel[];
  onCountrySelect: (country: CountryModel) => void;
}

const AutoComplete: FunctionComponent<OwnProps> = ({
  countries,
  onCountrySelect,
}) => {
  const onOptionChange = (event: any, country: any) => {
    event.preventDefault();
    onCountrySelect(country);
  };

  return (
    <StyledAutocomplete
      options={countries as CountryModel[]}
      getOptionLabel={(option: any) => option.name}
      onChange={onOptionChange}
      renderOption={(option: any) => (
        <React.Fragment>
          <StyledCountryWrapper>
            {countryToFlag(option.iso2Code)}
            <CountryText>{option.name}</CountryText>+{option.phone}
          </StyledCountryWrapper>
        </React.Fragment>
      )}
      renderInput={(params) => {
        return <TextField {...params} label="Combo box" variant="outlined" />;
      }}
    />
  );
};

export default AutoComplete;
