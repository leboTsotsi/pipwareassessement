import React, { FunctionComponent, MouseEvent, useState } from "react";
import styled from "styled-components";
import { Paper, Typography } from "@material-ui/core";
import { Country } from "../../store/countries";
import has from "lodash/has";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import { countryToFlag } from "../../utils/general";

const StyledContainer = styled(Paper)`
  && {
    border-radius: 10px;
    margin: 5px;
    padding: 5px;
    height: 300px;
    border: 0.5px solid #f9f9f9;
    &:hover {
      border: 0.5px solid red;
      background: pink;
    }
  }
`;

const ContentWrapper = styled.div`
  display: grid;
`;

const StyledTypography = styled(Typography)`
  && {
    float: left;
    font-family: MuseoSans-700;
    font-size: 24px;
    color: #4c4c4e;
    line-height: 34px;
  }
`;

const LabelValueWrapper = styled.div`
  display: flex;
`;

const Label = styled.div`
  font-family: MuseoSans-500;
  font-size: 14px;
  color: #4c4c4e;
  line-height: 34px;
  text-align: left;
`;

const Value = styled.div`
  font-family: MuseoSans-300;
  font-size: 14px;
  color: #868788;
  line-height: 34px;
  text-align: left;
  margin-left: 2.5px;
`;

const CloseButtonWrapper = styled.div<HoveringProps>`
  float: right;
  margin-right: 0px;
  ${(props) => (!props.isHovering ? "display: none" : "")};
`;

const StyledCloseButton = styled(IconButton)`
  && {
    margin: 0;
    padding: 0;
    :hover {
      background: transparent;
    }
  }
`;

interface HoveringProps {
  isHovering: boolean;
}

interface OwnProps {
  country: Country;
  onCountryDelete: (countryId: string) => void;
}

type AllProps = OwnProps;

const CountryListItem: FunctionComponent<AllProps> = ({
  country,
  onCountryDelete,
}) => {
  const [isHovering, setHoveringState] = useState(false);
  const toogleIsHoveringState = (event: MouseEvent): void => {
    event.preventDefault();
    setHoveringState(!isHovering);
  };

  return (
    <StyledContainer
      onMouseEnter={toogleIsHoveringState}
      onMouseLeave={toogleIsHoveringState}
    >
      <CloseButtonWrapper
        {...{ isHovering }}
        onClick={() => onCountryDelete(country.iso2Code)}
      >
        <StyledCloseButton edge="end" aria-label="close">
          <CloseIcon style={{ fontSize: "25px", color: "#979797" }} />
        </StyledCloseButton>
      </CloseButtonWrapper>

      <ContentWrapper>
        <StyledTypography>
          {country.name} {"   "} {countryToFlag(country.iso2Code)}
        </StyledTypography>
        <LabelValueWrapper>
          <Label>Capital City:</Label>
          <Value>{country.capitalCity}</Value>
        </LabelValueWrapper>
        <LabelValueWrapper>
          <Label>Income Level:</Label>
          <Value>{country.incomeLevel.value}</Value>
        </LabelValueWrapper>
        <LabelValueWrapper>
          <Label>Lending Type:</Label>
          <Value>{country.lendingType.value}</Value>
        </LabelValueWrapper>
        <LabelValueWrapper>
          <Label>Region:</Label>
          <Value>{country.region.value}</Value>
        </LabelValueWrapper>
        <LabelValueWrapper>
          <Label>Admin region:</Label>
          <Value>
            {has(country, "adminregion.value") &&
            country.adminregion.value !== ""
              ? country.adminregion.value.trim()
              : "Not specified"}
          </Value>
        </LabelValueWrapper>
      </ContentWrapper>
    </StyledContainer>
  );
};

export default CountryListItem;
