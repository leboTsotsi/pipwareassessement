import React, { FunctionComponent } from "react";
import styled from "styled-components";

interface RootProps {
  className?: string;
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  background-color: "#f9f9f9";
  height: 100%;
`;

const Root: FunctionComponent<RootProps> = ({ children }) => (
  <Wrapper>{children}</Wrapper>
);

export default Root;
