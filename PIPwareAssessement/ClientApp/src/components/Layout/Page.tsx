import React, { FunctionComponent } from "react";

import MuiGrid, { GridProps } from "@material-ui/core/Grid";

import styled from "styled-components";

const Grid = styled(MuiGrid as React.FunctionComponent<GridProps>)`
  height: 100%;
`;

const Page: FunctionComponent<{}> = ({ children }) => (
  <Grid container={true} direction="row">
    {children}
  </Grid>
);

export default Page;
