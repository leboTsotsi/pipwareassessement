export type ApiResponse = Record<string, any>;

export interface ResultType {
  id: string;
  iso2Code: string;
  value: string;
}

export interface Country extends ApiResponse {
  id: string;
  iso2Code: string;
  name: string;
  region: ResultType;
  adminregion: ResultType;
  incomeLevel: ResultType;
  lendingType: ResultType;
  capitalCity: string;
  longitude: string;
  latitude: string;
}

export interface CountryModel {
  iso2Code: string;
  name: string;
  phone: string;
}

export interface CountriesState {
  readonly allCountries: Country[];
  readonly countries: Country[];
  readonly errors?: string;
  readonly loading: boolean;
}
