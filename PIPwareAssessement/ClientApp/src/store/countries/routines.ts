import { createRoutine, promisifyRoutine } from "redux-saga-routines";

export const getCountries = createRoutine("@@countries/GET_COUNTRIES");
export const getCountriesPromise = promisifyRoutine(getCountries);

export const deleteById = createRoutine("@@countries/DELETE_BY_ID");
export const deeleteByIdPromise = promisifyRoutine(deleteById);

export const addCountry = createRoutine("@@countries/ADD_COUNTRY");
export const addCountryPromise = promisifyRoutine(addCountry);
