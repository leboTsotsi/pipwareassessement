import { AnyAction } from "redux";
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import callApi from "../../utils/callApi";
import { errorHandler } from "../../utils/errorHandler";
import { enqueueSnackbar } from "../notifications";
import {
  addCountry,
  deleteById,
  getCountries,
} from "./routines";
import { CountryModel } from "./types";

function* handleGetCountries(action: AnyAction) {
  try {
    const res = yield call(callApi, "get", "/Country/GetCountriesAsync", {});
    yield put(getCountries.success(res.data));
  } catch (err) {
    if (err.response) {
      yield put(getCountries.failure(errorHandler(err.response)));
    } else {
      yield put(getCountries.failure("An unknown error occured"));
    }
  } finally {
    yield put(getCountries.fulfill());
  }
}

function* handleAddNewCountry(action: AnyAction) {
  try {
    const country: CountryModel = action.payload.country;
   
    const res = yield call(callApi, "post", "/Country/AddCountry", {
      data: country,
    });
    yield put(addCountry.success(res.data));
  } catch (err) {
    if (err.response) {
      yield put(addCountry.failure(errorHandler(err.response)));
    } else {
      yield put(addCountry.failure("An unknown error occured"));
    }
  } finally {
    yield put(addCountry.fulfill());
  }
}

function* handleDeleteById(action: AnyAction) {
  try {
    const iso2Code = action.payload.iso2Code;
    const res = yield call(
      callApi,
      "delete",
      `/Country/DeleteByIso2Code?iso2Code=${iso2Code}`
    );
    yield put(deleteById.success(res.data));
  } catch (err) {
    if (err.response) {
      yield put(deleteById.failure(errorHandler(err.response)));
    } else {
      yield put(deleteById.failure("An unknown error occured"));
    }
  } finally {
    yield put(deleteById.fulfill());
  }
}

function* handleGetCountriesWatcher() {
  yield takeEvery(getCountries.TRIGGER, handleGetCountries);
}

function* handleAddCountryWatcher() {
  yield takeEvery(addCountry.TRIGGER, handleAddNewCountry);
}

function* handleDeleteByIdWatcher() {
  yield takeEvery(deleteById.TRIGGER, handleDeleteById);
}

// Error handlers
function* handleCountryError(action: AnyAction) {
  yield put(
    enqueueSnackbar({
      message: action.payload,
      options: {
        variant: "error",
      },
    })
  );
}

function* countriesErrorWatcher() {
  yield takeEvery([getCountries.FAILURE], handleCountryError);
}

export function* countriesSaga() {
  yield all([
    fork(handleGetCountriesWatcher),
    fork(countriesErrorWatcher),
    fork(handleDeleteByIdWatcher),
    fork(handleAddCountryWatcher),
  ]);
}
