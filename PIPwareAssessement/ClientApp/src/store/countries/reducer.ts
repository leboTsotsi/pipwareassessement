import { Reducer } from "redux";
import { deleteById, getCountries } from "./routines";
import { CountriesState, Country } from "./types";

const initialState: CountriesState = {
  allCountries: [],
  countries: [],
  errors: undefined,
  loading: false,
};

const reducer: Reducer<any> = (state = initialState, action) => {
  switch (action.type) {
   

    case getCountries.TRIGGER: {
      return { ...state, countries: [], errors: undefined, loading: true };
    }

    case deleteById.TRIGGER: {
      return { ...state, errors: undefined, loading: true };
    }

    case getCountries.SUCCESS: {
      return {
        ...state,
        countries: action.payload[1],
        errors: undefined,
        loading: false,
      };
    }

    case deleteById.SUCCESS: {
      return { ...state, errors: undefined, loading: false };
    }

    case deleteById.FAILURE:
    case getCountries.FAILURE: {
      return { ...state, errors: action.payload, loading: false };
    }

    case deleteById.SUCCESS:
    case getCountries.FULFILL: {
      return { ...state, loading: false };
    }

    default: {
      return state;
    }
  }
};

const removeCountryFromState = (iso2Code: string, countries: Country[]) => {};

export { reducer as countriesReducer };
