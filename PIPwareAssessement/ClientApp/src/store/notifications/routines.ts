import { createRoutine } from 'redux-saga-routines';

export const enqueueSnackbar = createRoutine(
  "@@notifications/ENQUEUE_SNACKBAR"
);
export const removeSnackbar = createRoutine("@@notifications/REMOVE_SNACKBAR");
export const openDialog = createRoutine(
  "@@notifications/ENQUEUE_ALERT"
);
