export interface Notification {
  key: string;
  message: string;
  options?: any;
}

export interface NotificationState {
  notifications: Notification[];
}
