import { connectRouter, RouterState } from "connected-react-router";
import { History } from "history";
import { Action, AnyAction, combineReducers, Dispatch } from "redux";
import { routinePromiseWatcherSaga } from "redux-saga-routines";
import { all, fork } from "redux-saga/effects";
import { countriesReducer, countriesSaga, CountriesState } from "./countries";
import {
  notificationReducer,
  notificationSaga,
  NotificationState,
} from "./notifications";

export interface ApplicationState {
  countries: CountriesState;
  router: RouterState;
  notifications: NotificationState;
}

// Additional props for connected React components. This prop is passed by default with `connect()`
export interface ConnectedReduxProps<A extends Action = AnyAction> {
  dispatch: Dispatch<A>;
}

// Whenever an action is dispatched, Redux will update each top-level application state property
// using the reducer with the matching name. It's important that the names match exactly, and that
// the reducer acts on the corresponding ApplicationState property type.
export const rootReducer = (history: History) =>
  combineReducers<ApplicationState>({
    countries: countriesReducer,
    router: connectRouter(history),
    notifications: notificationReducer,
  });

// Here we use `redux-saga` to trigger actions asynchronously. `redux-saga` uses something called a
// "generator function", which you can read about here:
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/function*
export function* rootSaga() {
  yield all([
    fork(routinePromiseWatcherSaga),
    fork(countriesSaga),
    fork(notificationSaga),
  ]);
}
