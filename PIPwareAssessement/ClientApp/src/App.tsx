import React from "react";
import { Route } from "react-router";
import Root from "./components/Layout/Root";
import Home from "./Pages/Home";

import { ConnectedRouter } from "connected-react-router";
import { History } from "history";
import { Provider } from "react-redux";
import { Store } from "redux";
import { ApplicationState } from "./store";

// Any additional component props go here.
interface OwnProps {
  store: Store<ApplicationState>;
  history: History;
}

// Create an intersection type of the component props and our Redux props.
type AllProps = OwnProps;

const App: React.FunctionComponent<AllProps> = ({ history, store }) => {
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <Root>
          <Route exact path="/" component={Home} />
        </Root>
      </ConnectedRouter>
    </Provider>
  );
};

export default App;
