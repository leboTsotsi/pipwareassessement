import axios from "axios";

// todo: read from enviroment variables
const API_URL = "https://localhost:5001/api/";

const API = axios.create({
  baseURL: API_URL,
  withCredentials: true,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

export default function callApi(method: string, path: string, options?: any) {
  const axiosOptions = {
    data: {},
    method,
    params: {},
    url: path,
    ...options,
  };

  return API(axiosOptions);
}
