import { createBrowserHistory } from "history";
import * as React from "react";
import * as ReactDOM from "react-dom";

import configureStore from "./configureStore";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";

const history = createBrowserHistory({
  basename: process.env.REACT_APP_BASENAME,
});
const store = configureStore(history);

ReactDOM.render(
  <React.Fragment>
    <App store={store} history={history} />
  </React.Fragment>,
  document.getElementById("root")
);
registerServiceWorker();
