import React, {
  FunctionComponent,
  SyntheticEvent,
  useEffect,
  useState,
} from "react";
import { connect } from "react-redux";
import { Content, Page } from "../components/Layout";
import styled from "styled-components";
import Paper, { PaperProps } from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import AddIcon from "@material-ui/icons/Add";
import IconButton from "@material-ui/core/IconButton";
import { bindActionCreators, Dispatch } from "redux";
import { bindPromiseCreators } from "redux-saga-routines";
import { ApplicationState } from "../store";
import {
  addCountryPromise,
  Country,
  deeleteByIdPromise,
  getAllCountriesPromise,
  getCountriesPromise,
  CountryModel,
} from "../store/countries";
import CountryView from "../components/views/CountryView";
import Button from "@material-ui/core/Button";
import { Dialog, DialogContent, DialogActions } from "@material-ui/core";
import AutoComplete from "../components/ui/AutoComplete";
import { allCountries } from "../utils/general";
import has from "lodash/has";

const StyledContent = styled(Content)`
  max-width: 1024px;
  margin: 0 auto !important;
  width: 100%;
`;

const StyledPaper = styled(Paper as FunctionComponent<PaperProps>)`
  && {
    border-radius: 4px;
    margin-top: 58px;
    padding: 23px 20px 20px;
    width: 100%;
    border: 0.5px solid #f9f9f9;
  }
`;

const StyledTypography = styled(Typography)`
  && {
    font-family: MuseoSans-700;
    font-size: 20px;
    color: #4c4c4e;
    line-height: 34px;
  }
`;

const AddButtonWrapper = styled.div`
  float: right;
  margin-right: 0px;
`;

const StyledAddButton = styled(IconButton)`
  && {
    margin: 0;
    padding: 0;
    :hover {
      background: transparent;
    }
  }
`;

const StyledAddIcon = styled(AddIcon)`
  && {
    font-size: 34px;
    color: #4c4c4e;
  }
`;

const LoadingState = styled.div`
  justify-content: center;
  text-align: center;
`;

const EmptyState = styled.div`
  justify-content: center;
  text-align: center;
  margin-top: 20px;
  display: flex;
`;

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  border: 0.5px solid #b0b1b3;
  border-radius: 5px;
  padding: 4px;
`;

const StyledCountryWrapper = styled.div`
  display: flex;
`;

const CountryText = styled(Typography)`
  && {
    font-family: MuseoSans-700;
    font-size: 16px;
    color: #4c4c4e;
    line-height: 28px;
    margin-left: 5px;
    margin-right: 5px;
    margin-top: -2.5px;
  }
`;

const AddCountryButton = styled(Button)`
  && {
    background: #b0b1b3;
    margin-left: 10px;
  }
`;

interface PropsFromState {
  countries: Country[];
  loading: boolean;
}

interface PropsFromDispacth {
  handleGetCountries: typeof getCountriesPromise;
  handleCountryDeleteById: typeof deeleteByIdPromise;
  handleGetAllCountries: typeof getAllCountriesPromise;
  handleAddNewContry: typeof addCountryPromise;
}

type AllProps = PropsFromDispacth & PropsFromState;

const Home: FunctionComponent<AllProps> = ({
  countries,
  loading,
  handleGetCountries,
  handleGetAllCountries,
  handleCountryDeleteById,
  handleAddNewContry,
}) => {
  const [dialogOpen, setDialogOpen] = useState(false);
  const [selectedCountry, setSelectedCountry] = useState({});

  const onCountryDelete = (iso2Code: string): void => {
    handleCountryDeleteById({ iso2Code }).then(() => {
      handleGetCountries({});
    });
  };

  const onCountryAdd = (event: SyntheticEvent): void => {
    event.preventDefault();
    if (has(selectedCountry, "name")) {
      handleAddNewContry({ country: selectedCountry }).then(() => {
        handleGetCountries({});
        onDialogClose();
      });
    }
  };

  const onCountrySelect = (country: CountryModel): void => {
    setSelectedCountry(country);
  };

  const onAddClick = (event: SyntheticEvent): void => {
    event.preventDefault();
    setDialogOpen(true);
  };

  const onDialogClose = (): void => {
    setSelectedCountry({});
    setDialogOpen(false);
  };

  const onCancel = (event: SyntheticEvent): void => {
    event.preventDefault();
    onDialogClose();
  };

  useEffect(() => {
    // componentDidMount
    handleGetCountries({});
    return () => {
      // componentWillUnmount
    };
  }, [handleGetCountries, handleGetAllCountries]);

  if (loading) {
    return (
      <LoadingState>
        <StyledTypography>still loading...</StyledTypography>
      </LoadingState>
    );
  }

  return (
    <Page>
      <StyledContent>
        <StyledPaper>
          <Header>
            <StyledTypography>Country Dashboard</StyledTypography>
            <AddButtonWrapper>
              <StyledAddButton
                onClick={onAddClick}
                edge="end"
                aria-label="close"
              >
                <StyledAddIcon />
              </StyledAddButton>
            </AddButtonWrapper>
          </Header>
          {!loading && countries ? (
            <CountryView {...{ countries, onCountryDelete }} />
          ) : (
            <EmptyState>
              <StyledTypography>There are no countries.</StyledTypography>
              <AddCountryButton onClick={onAddClick}>Add New</AddCountryButton>
            </EmptyState>
          )}
        </StyledPaper>
        <Dialog open={dialogOpen}>
          <DialogContent>
            <AutoComplete {...{ countries: allCountries, onCountrySelect }} />
          </DialogContent>
          <DialogActions>
            <Button onClick={onCountryAdd}>Save</Button>
            <Button onClick={onCancel}>Cancel</Button>
          </DialogActions>
        </Dialog>
      </StyledContent>
    </Page>
  );
};

const mapStateToProps = ({ countries }: ApplicationState) => ({
  countries: countries.countries,
  loading: countries.loading,
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  ...bindActionCreators({}, dispatch),
  ...bindPromiseCreators(
    {
      handleAddNewContry: addCountryPromise,
      handleCountryDeleteById: deeleteByIdPromise,
      handleGetCountries: getCountriesPromise,
    },
    dispatch
  ),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
