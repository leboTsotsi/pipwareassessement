﻿using System;
using System.Linq;
namespace PIPwareAssessement.Models
{
    public class Country
    {
        public Guid id { get; set; }
        public string name { get; set; }
        public string iso2Code { get; set; }
    }
}
